package com.hw.droid.hwcatalog;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import hwdroid.app.HWListActivity;
import hwdroid.widget.ItemCursorAdapter;
import hwdroid.widget.item.Item;
import hwdroid.widget.item.Item.Type;
import hwdroid.widget.item.Text2Item;
import hwdroid.widget.itemview.ItemView;

public class ItemCursorAdapterSamples extends HWListActivity {
    private ItemCursorAdapter mAdapter;
    private Cursor mCursor;
    private static final String[] PROJECT = {
            "_id", "TEXT", "SUBTEXT"
    };

	private CheckBox mAllCheckBox;
	private boolean mIsSwitchChecked = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCursor = createDummyCursor();
        mAdapter = new TestItemCursorAdapter(this, mCursor, Item.Type.CHECK_MODE);
        setListAdapter(mAdapter);
        
	    mAllCheckBox = new CheckBox(this);    	
	    mAllCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				if(mIsSwitchChecked) {
					mAdapter.doAllSelected(arg1);
					mAdapter.notifyDataSetChanged();
					
					setTitle2("selected = "+ mAdapter.getSelectedCounts());
				}
				
				mIsSwitchChecked = true;
			}});
    	
    	this.setRightWidgetView(mAllCheckBox); 	
    }

    private Cursor createDummyCursor() {
        MatrixCursor cursor = new MatrixCursor(PROJECT);
        for(int i = 0; i < 100; i ++) {
            cursor.addRow(new Object[]{String.valueOf(i), "text" + i, "subtext" + i});
        }
        return cursor;
    }

    private class TestItemCursorAdapter extends ItemCursorAdapter {
        public TestItemCursorAdapter(Context context, Cursor c, Type type) {
            super(context, c, type);

        }

        @Override
        public Item setupItem(Context context, Cursor cursor) {
            return new Text2Item(cursor.getString(1), cursor.getString(2));
        }
    }

    protected void onListItemClick(ListView l, View v, int position, long id) {
        ItemView view = (ItemView) v;

        Item item = (Item) v.getTag();
        item.setChecked(!item.isChecked());
        view.setObject(item);
        ItemCursorAdapter adapter = (ItemCursorAdapter)l.getAdapter();
        adapter.setSelectedItem(position);
    	
    	setTitle2("selected = "+ adapter.getSelectedCounts());
    	
    	if(adapter.getSelectedCounts() == adapter.getCount()) {
    		if(!mAllCheckBox.isChecked()) {
    			mIsSwitchChecked = false;
    		    mAllCheckBox.setChecked(true);
    	    	
    		}
    	} else {
    		if(mAllCheckBox.isChecked()) {
    			mIsSwitchChecked = false;
    		    mAllCheckBox.setChecked(false);
    	    	
    		}
    	}
    }
}
