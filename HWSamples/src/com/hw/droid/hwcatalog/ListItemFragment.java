package com.hw.droid.hwcatalog;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.hw.droid.hwcatalog.indicator.FlexibleTabIndicatorActivity;

import hwdroid.widget.ItemAdapter;
import hwdroid.widget.item.DrawableText2Item;
import hwdroid.widget.item.Item;
import hwdroid.widget.item.Item.Type;
import hwdroid.widget.itemview.ItemView;

import java.util.ArrayList;
import java.util.List;

public final class ListItemFragment extends Fragment {
    private static final String KEY_TYPE = "ListItemFragment:Type";
    private static final String KEY_LISTENER = "ListItemFragment:Listener";
    private ListView mListView;
    
    int postion = 0;
    
    public interface OnScrollListener2 {
    	void OnScrollListenr(boolean location);
    };
    

    public static ListItemFragment newInstance(String content, int type) {
    	ListItemFragment fragment = new ListItemFragment();

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 20; i++) {
            builder.append(content).append(" ");
        }
        builder.deleteCharAt(builder.length() - 1);
        fragment.type = type;

        return fragment;
    }
    
    private int type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            if(savedInstanceState.containsKey(KEY_TYPE)) 
                type = savedInstanceState.getInt(KEY_TYPE);
        }

        mListView = new ListView(this.getActivity());
    	mListView.setOnScrollListener(new OnScrollListener(){

			@Override
			public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
			    
			    if(null == FlexibleTabIndicatorActivity.indicator) return;
			    
				if(postion == 0 && arg1 > 0) {
					postion = arg1;
				}
				
				if(arg1 - postion > 1) {
				    FlexibleTabIndicatorActivity.indicator.toggleShrinkTabs();
				} else if(arg1 - postion < 0) {
				    FlexibleTabIndicatorActivity.indicator.toggleExpandTabs();
				}
			}

			@Override
			public void onScrollStateChanged(AbsListView arg0, int arg1) {
				switch (arg1) {  
	            case OnScrollListener.SCROLL_STATE_IDLE: //  
	            	postion = 0;
	                break;
	            } 
			}});
    	
    	mListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
		    	ItemView view = (ItemView) arg1;
		    	
		    	ItemAdapter adapter = (ItemAdapter)mListView.getAdapter();
		    	Item item = (Item)adapter.getItem(arg2);
		    	item.setChecked(!item.isChecked());
		    	view.setObject(item);
			}});
    	
    	List<Item> items = new ArrayList<Item>();
    	
	    for(int i = 0; i < 1000; i ++) {
	      if(type == 0) {
	          items.add(new DrawableText2Item("type 0" + i, "subtext" + i, this.getResources().getDrawable(R.drawable.aui2_5)));
	      } else if(type == 1) {
	    	  items.add(new DrawableText2Item("type 1" + i, "subtext" + i, this.getResources().getDrawable(R.drawable.aui2_5)));
	      } else if(type == 2) {
	    	  items.add(new DrawableText2Item("type 2" + i, "subtext" + i, this.getResources().getDrawable(R.drawable.aui2_5)));
	      } else {
	    	  items.add(new DrawableText2Item("type 4" + i, "subtext" + i, this.getResources().getDrawable(R.drawable.aui2_5)));
	      }
	    }
	  
	    ItemAdapter adapter = new ItemAdapter(this.getActivity(), items);
	    
	    if(type == 0) {
	    	adapter.setTypeMode(Type.RADIO_MODE);
	    } else if(type == 1) {
	    	adapter.setTypeMode(Type.CHECK_MODE);
	    } else if(type == 2) {
	    	adapter.setTypeMode(Type.IMAGE_MODE);
		    for(int i = 0; i < 1000; i ++) {
			      DrawableText2Item item = (DrawableText2Item)items.get(i);
			      item.rightDrawable = this.getActivity().getResources().getDrawable(R.drawable.aui2_5);
			}
	    	
	    } else {
	    	adapter.setTypeMode(Type.NORMAL_MODE);
	    }
	    
	    mListView.setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup parent = (ViewGroup) mListView.getParent();
        if (parent != null) {
            parent.removeView(mListView);
        }

        return mListView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        outState.putInt(KEY_TYPE, type);
    }
}
